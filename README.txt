Project Name: Radiology Java Standalone Application

Function: Takes 3 inputs through a file chooser:
1. One or more Reports for processing
2. Keyword file to check for all the keyword present in a new line
3. The output folder.

Output:
The output folder in which doc files of all selected reports will be created with highlighted keyword bearing any negative comorbodity in a sentence.

Prerequisites:
1. Java Version 8.0 or greater with Swing functionality enabled. 

How to run:
1. Open the folder after downloading the folder "Radiology_Java_App_Srikant_Panda".
2. Copy the path of the folder <PATH_NAME>.
3. Open the Radiology.bat file in text editor.
4. Replace the first line with "cd <PATH_NAME>"
5. close the BAT file
6. Run the BAT file
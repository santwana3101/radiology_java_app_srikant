Associated Diagnoses: None
Author: SAVLA , GEETA

Interval History
s/p POD#7 Left cranioplasty without complaints. drains removed by neurosurgery. per neurosurgery, holding anticoagulation for now atleast for 14 days per their note from today

no overnight concerns. OT consulted. nursing to provide more means to communicate

Hospital Course
56yoM with PMHx nonischemic cardiomyopathy (EF 40%), HTN, HLD, DMII, Pulmonary HTN, polysubstance use (cocaine/cannibus), initially admitted to CACU 9/27 for decompensated heart failure and newly diagnosed Afib. Positive cocaine found on urine toxicology. On 9/28, inpatient CODE BAT called after patient found with aphasia and right sided hemiplegia, did not receive IV tPa as onset of symptom time was undetermined, CT head already with completed large left MCA stroke. CTA head/neck with occlusion of Left ICA with distal reconstitution, occlusion of left M1 segment of Left MCA with limited leptomeningeal collaterals. Patient then transferred to Neuro ICU, after repeat imaging with increased edema and mass effect, patient taken for prophylactic left decompressive hemicraniectomy on 9/29, was on hypertonic saline for management of cerebral edema, intubated and sedated. Repeat CT head 9/30 with hemorrhagic transformation of stroke. Video EEG initially with LRDA+S for which Keppra was started with resolution of sharp waves, repeat EEG on 10/18 with generalized background slowing, attenuation of rhythm and voltage in the left hemisphere, suggestive of a moderate encephalopathy with focal cerebral dysfunction in the left hemisphere. Patient underwent PEG and Trach placement on 10/05 (trach placed by Dr. Paccione). During course of admission, patient had episodes of fever, wound culture grew propionibacterium acnes (isolated from broth only), sputum culture with staph and h. influenza, received vancomycin, meropenem and cefepime. Cardiology continues to follow patient for management of CHF, Afib and episode of Vtach while in Neuro ICU.
Repeat CT head 10/12, without intracranial hemorrhage. Patient started on Heparin gtt 10/17 and transferred to Stroke Unit for further management of stroke. Patient started on Coumadin 10/21 after Neurosurgery reported okay to start anticoagulation. Patient noted to be lethargic overnight on 10/22, repeat CT head concerning for hemorrhagic conversion, Heparin gtt bridging to Coumadin discontinued and ASA was suspended. Repeat CT head 10/24 with stable hemorrhagic conversion, started back on ASA 81mg daily for stroke prevention as per stroke attending. CT head repeated 11/1, with interval decrease in hemorrhagic conversion of left MCA/PCA stroke. ASA discontinued and start Heparin gtt as per Stroke attending, no bolus plan with goal aPTT 45-60. ASA resumed 11/6, discontinued 11/8. Started Coumadin 11/8, INR pending this AM. Heparin gtt discontinued 11/18 as with therapeutic INR. Repeat CT head 11/8 without hemorrhagic conversion, will continue to monitor hypodense subgaleal fluid collection next to craniectomy site. Started back on bridge to Coumadin with Lovenox SQ on 11/22 as INR subtherapeutic, will plan to discontinue once INR back at goal 2-3.
Echo on 9/28 with EF 26%, negative bubble study, with severely increased LV cavity size, severely reduced global LV systolic function with diffuse hypokinesis, segmental wall motion abnormalities, severe diastolic dysfunction with elevated LV filling pressures, severely dilated left atrium, mildly enlarged RV, moderately dilated right atrium, mildly reduced RV systolic function, tethering of MV leaflets, trace MR, dilated aortic arch, with modearte decrease in LV systolic function since prior study. Repeat Echo 10/17 with EF 40%, moderately increase LV wall thickness, moderately to severely reduced global LV systolic function with diffuse hypokinesis, severe diastolic dysfunction with elevated LV filling pressures, trace MR/TR. Bilateral venous and arterial duplex negative.
Trauma Surgery removed 4 trach sutures at bedside on 10/18. Trauma Surgery consulted regarding trach change - patient is s/p trach change on 10/25, SLP assessed patient with speaking valve in place, with good airflow. Trach should not be occluded overnight as per surgery (ex: no speaking valve nor trach cap overnight). Trach inadvertently removed on October 26, Nov 2, Nov 3 and Nov 5, dislodged Nov 7, replaced with no complication by respiratory. Notified 11/8 that Trauma Surgery okay with Respiratory to proceed with decannulation, s/p decannulation 11/10 with respiratory, O2 sats remain stable.
SLP to follow regarding communication progress. SLP recommending advanced with thin liquid dysphagia diet, will continue to monitor PO intake, PEG feeds discontinued with plan for calorie count with nutrition to assess for adequate PO intake, will folow up 11/24.
CT head 9/28: Large left MCA territory infarct and possibly also involving a portion of the left PCA territory.
CTP 9/28:Large left MCA territory and partial PCA territory cortical infarct as described above.
CTA neck 9/28: total occlusion of the left ICA.
CTA head 9/28: Occlusion of the left internal carotid artery with distal reconstitution. Occlusion of the left M1 segment of the middle cerebral artery with limited leptomeningeal collaterals. Decreased branching of L PCA which arises from L ICA.
Echo 9/28: EF 26% Severely reduced global left ventricular systolic function with diffuse hypokinesis. Segmental wall motion abnormalities. Severe diastolic dysfunction. Severely dilated left atrial size. no shunt.
LE Venous duplex 9/30: Conclusion:Venous duplex study of the bilateral lower extremities showed no evidence ofdeep vein thrombosis.
CT head 9/30:
1. Mild further increased edema and mass effect in the very large left hemispheric infarct without evidence of hemorrhagic transformation.
2. Status post left sided craniectomy again seen with increased hemorrhage and fluid collection, decreased air locules along the craniotomy site.
LE Venous duplex 10/07: Conclusion: Venous duplex study of the right and left lower extremities showed no evidence of deep vein thrombosis.
Echo 10/17: EF 40%, Moderately to severely reduced global left ventricular systolic function with diffuse hypokinesis. Severe diastolic dysfunction
LE Arterial duplex 10/17: Conclusion:Bilateral lower extremity arterial duplex was performed. Study showed no evidence of significant stenosis or flow disturbance. Limited study due due to patient moving.
CT head 11/5 :Stable appearance of an evolving infarction involving the left MCA and PCA territories with local mass effect in a patient status post left frontoparietal craniectomy as described. Stable punctate foci of hemorrhage without gross hemorrhagic conversion as described. Again seen is extradural hypodense fluid collection along the posterior aspect of the craniectomy site measuring up to 2.0 cm in width as described. Correlate clinically.
CT head 11/8: Interval increase in the size of the large hypodense subgaleal collection adjacent to the left-sided craniectomy site. Stable late subacute left MCA/PCA territory infarction. No hemorrhagic transformation.
CT head 12/9: old infarct, no new acute findings, no hemorrhage

Arterial duplex with all arteries patent however also shows multiple areas of stenosis. Will consider vascular consult non-emergently.
Neurosurgery planning cranioplasty on 1/4. Neurosurgery recs noted for type and screen and to call OR on 1/2 to book for 1/3. Will need to stop heparin 8 hours prior to surgery. Type an screen, 2 units of pRBCs and 2 units of PLT on call to OR and ancef on call to OR.
Patient completed treatment of UTI with ceftriaxone on 1/3. Repeat urine culture 1/1 negative.


Health Status
Problem list:
Active Problems (14)
At risk of venous thromboembolus (2674624010)
Atrial fibrillation (82343012)
Current smoker (128130017)
Diabetes mellitus (121589010)
Embolic left MCA stroke (300322016)
Heart failure (1234906013)
History of tobacco use (2966944018)
HLD (hyperlipidemia) (92826017)
HTN (hypertension) (1215744012)
Morbid obesity (356968010)
PEG (percutaneous endoscopic gastrostomy) status (443655017)
Stroke due to embolism of left middle cerebral artery (1209751018)
Stroke due to embolism of left posterior cerebral artery (1209751018)
Tracheostomy present (443654018)

Allergies:
Allergies (0) Active
NKA
Current medications:
Medications (25) Active
Continuous: (18)
atorvastatin 40 mg TAB 40 mg, Oral, Bedtime
cholecalciferol 1000 units TAB 1,000 Units, Oral, Once daily
Critic-Aid Clear Adult 1 application, Topical, TID
Docusate Sod 100mg Sftgl 100 mg, Oral, BID
famotidine 20 mg TAB 40 mg, Oral, Bedtime
Insulin Glargine 100 units/mL 15 Units, SubCutaneous, QAM
Insulin Lispro 100 units/mL INJ Per BG Level, SubCutaneous, AC
Insulin Lispro 100 units/mL INJ Per BG Level, SubCutaneous, Bedtime
Lactobacillus Acidophilus Cap 1 cap, Oral, TID
Lisinopril 5 mg TAB 5 mg, Oral, Once daily
metoprolol 25 mg TAB 25 mg, Oral, Q12H
Nystatin 100000 U/gm 10gm Powder 1 application, Topical, Q8H
Potassium Cl 20Meq/15mL UD 20 mEq, Oral, Once daily
Sertraline 50mg Tab 50 mg, Oral, Bedtime
Sodium Chloride 0.9% 1,000 mL 20 mL/hr, Continuous IV, Stop: 02/01/18 17:33:00
Warfarin (Coumadin) Patient Education and Daily Order Reminder 1 ea, Patient education and daily order reminder, MISC, INT-Q24H
Warfarin (Coumadin) Patient Education and Daily Order Reminder 1 ea, Patient education and daily order reminder, MISC, INT-Q24H
Warfarin (Coumadin) Patient Education and Daily Order Reminder 1 ea, Patient education and daily order reminder, MISC, INT-Q24H
PRN: (7)
0.9% NaCl 10 mL Inj 10 mL, IV Push, Q12H, PRN: Catheter Care
Acetaminophen 325mg Tab 650 mg, Oral, Q6H, PRN: Pain (Moderate 4-6)
Dextrose 50% Syringe 50 mL 15 g, IV Push, Q15MIN, PRN: Blood Sugar Less than 70 mg/dL
Glucagon 1mg (1 unit) Inj 1 mg, IntraMuscular, X1, PRN: Blood Sugar Less than 70 mg/dL
Glucose Oral Gel 15 g, Oral, Q15MIN, PRN: Blood Sugar Less than 70 mg/dL
guaiFENesin Dm 10mL ud 10 mL, Oral, Q4H, PRN: Cough
Ondansetron 2mg/mL 2mL Inj 4 mg, IV Push, Q6H, PRN: Nausea and Vomiting


Review of Systems
Constitutional: Negative except as documented in history of present illness.
Eye: Negative except as documented in history of present illness.
Ear/Nose/Mouth/Throat: Negative except as documented in history of present illness.
Respiratory: Negative except as documented in history of present illness.
Cardiovascular: Negative except as documented in history of present illness.
Gastrointestinal: Negative except as documented in history of present illness.
Genitourinary: Negative except as documented in history of present illness.
Musculoskeletal: Negative except as documented in history of present illness.
Integumentary: Negative except as documented in history of present illness.
Neurologic: Negative except as documented in history of present illness.
Psychiatric: Negative except as documented in history of present illness.

Physical Examination
Vital Signs and Measurements:
Vital Signs (last 24 hrs)_____ Last Charted___________
Temp Oral 36.5 DegC (JAN 11 09:41)
Resp Rate 16 br/min (JAN 11 09:42)
SBP 136 mmHG (JAN 11 09:58)
DBP 76 mmHG (JAN 11 09:58)
SpO2 96 % (JAN 11 09:42)
Weight 113.7 kg (JAN 11 11:12)
Height 184 cm (JAN 11 11:12)
.

General
Alert and oriented.
No acute distress.
Eye: Pupils are equal, round and reactive to light, Normal conjunctiva.
HENT: left craniotomy noted.
Neck: Supple, No jugular venous distention.
Respiratory: Lungs are clear to auscultation, Symmetrical chest wall expansion.
Cardiovascular: Normal rate, Regular rhythm.
Gastrointestinal: Soft, Normal bowel sounds, peg intact.
Genitourinary: No costovertebral angle tenderness.
Lymphatics: No lymphadenopathy neck, axilla, groin.
Musculoskeletal: No swelling, positive for right hemiplegia.
Integumentary: Warm, Dry, No pallor.
Neurologic: Alert and oriented X 3, positive for right hemiplegia and aphasia.
Psychiatric: Appropriate mood & affect.

Review / Management
Results review
Radiology results
Arterial duplex
Conclusion:
Right and left lower extremity arterial duplex exam was performed. Mildly calcified vessels were
visualized throughout. All arteries appeared patent. See above chart for obtained velocities. Two areas of
stenosis were visualized in the right proximal anterior tibial artery. Multiple collaterals were noted in the
right mid and distal anterior tibial artery. Reversal of flow was obtained in the right distal anterior tibial
artery and in the right dorsi pedis artery. A greater than 50% stenosis was also visualized in left proximal
anterior tibial artery. Collaterals were noted in the left mid anterior tibial artery. Dampened waveforms
with low velocities were obtained in the left dorsi pedis artery.

CT head (12/9)
IMPRESSION: Large left temporoparietal craniectomy defect. Line is
consistent with a old left MCA/PCA territory infarction. No
intracranial hemorrhage.

CT chest without IV contrast (10/18)
IMPRESSION:
1. New patchy and nodular opacities in the right middle lobe.
Additional nodular opacities are seen in the bilateral lower lobes as
above. Findings suggest infectious oe inflammatory etiology.
Follow-up chest CT in 6-8 weeks after appropriate treatment advised.
2. Nonspecific scattered areas of mosaic attenuation are seen which
may reflect air trapping.
3. Nonspecific mediastinal and right hilar adenopathy overall less
pronounced than on prior. Attention on follow up advised.
4. Cardiomegaly.
5. Atherosclerotic calcifications of the aorta.
6. Lines and tubes as described.
.
Microbiology results

Impression and Plan
56 years old white male PMHx nonischemic cardiomyopathy (EF 40%), HTN, HLD, DMII, Pulmonary HTN, polysubstance use (cocaine/cannibus), initially admitted to CACU 9/27 for decompensated heart failure and newly diagnosed Afib. Positive cocaine found on urine toxicology. On 9/28, inpatient CODE BAT called after patient found with aphasia and right sided hemiplegia, did not receive IV tPa as onset of symptom time was undetermined, CT head already with completed large left MCA stroke.

CVA s/p left craniectomy with hemorrhagic conversion with right hemiplegia
-Mental status stable
-Repeat CT head (12/9) stable, no new acute findings and no hemorrhage
-STAT CT head if any change in neurological status
-s/p POD#7 Left cranioplasty without complaints. drains removed
-neurosurgery closely following the pt
-anticoagulation stopped per neurosurgery for atleast 14 days from cranioplasty

Afib/CHF/HTN/HLD
-c/w metoprolol for rate control
-awaiting neurosurgery recs for anticoagulation, only mentioned sc heparin which we started
-continue w/ lisinopril 5mg daily and metoprolol to 25mg q12hrs for BP and rate control
-continue w/ atorvastatin 40mg daily for HLD and stroke prevention

Type 2 DM
-HA1C 7.7
-Continues on Lantus 30 U and low dose correction scale
-Holding Metformin 1000mg BID 12/01
-SLP recommending advanced with thin liquid dysphagia diet, will continue to monitor PO
-Nutrition consulted for repeat calorie count as of 11/24 been off tube feed
--Encourage PO intake

Acute on chronic respiratory failure s/p Trach 10/05 and trach removal-> resolved
-saturating well on RA
-s/p trach decannulation by Respiratory 11/10
-continue budesonide

Depression
-Continue Zoloft

Prevention/Misc:
-multivitamin supplementation
-Nystatin powder to abdominal fold
-Vitamin d deficiency - 7 , continue supplementation, repeat Vitamin D level after 8 weeks of treatment

Abnormal Ct-Chest:
-Repeat CT Chest in 6-8 weeks (from Oct 18 2017) to follow-up on incidental CT Chest findings of patchy and nodular opacities in the right middle lobe, additional nodular opacities in the bilateral lower lobes
-CT chest was planned for outpatient setting but since the patient will be here as inpatient until guardianship, so it was repeated as inpatient, and suggested pneumonitis, no fever, normal WBC. we will monitor for now.

Dispo: Patient awaiting guardianship

DVT prophylaxis - anticoagulated, SCD, OOB to chair with assist; fall risk




Discharge Plan
Equipment and Care Needs for Discharge
Support Assessment: Date of Most Recent Assessment 01/06/2018.

Anticipated Discharge
Discharge Date Pending
Portable view of the chest


Clinical indication is myocardial infarction


Comparison is made to 3/1/2018


FINDINGS: There is stable cardiomegaly. There is no pneumothorax.

There is no pleural effusion. There is no significant pulmonary

congestion.


IMPRESSION: Cardiomegaly without evidence of CHF or airspace disease.


The preceding interpretation was performed and electronically signed

by DR. CLIFF BERNSTEIN on 3/4/2018 5:51 PM

 

Attending Radiologist:     BERNSTEIN, CLIFF

Ordered By:                ZHONG, KAI

Order Date/Time:           March 4, 2018 4:10 PM

 

 

      

***Not Official Copy***:  CT:Chest/angio W/contrast

Date/Time of Service:     March 01, 2018 22:45

Result status:                 Final

Result title:                    CT CHEST ANGIO WO AND WITH IV CONTRAST

Contributor System:       RADIOLOGY
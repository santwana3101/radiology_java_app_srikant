Exam: CHEST AP PORTABLE


CLINICAL HISTORY: Female, 63 years old.

63 YO F W/PERICARDIAL EFFUSION S/P PERICARDIAL WINDOW AND PLEURAL

DRAIN

REASON: EVALUATE FOR PNEUMOTHORAX.

.


TECHNIQUE: CHEST AP PORTABLE


COMPARISON: 3/1/2018


FINDINGS:

Lines/Tubes: Right Upper extremity PICC with tip near the cavoatrial

junction..

Lungs and pleura: There is no pulmonary vascular congestion. Patchy

lingular atelectasis.

Possible trace left-sided pleural effusion. There is no pneumothorax.

Mediastinum: The cardiomediastinal silhouette is enlarged and

unchanged. Loop recorder device overlying the cardiac silhouette.


IMPRESSION: No significant interval change.


I, DR. CLIFF BERNSTEIN, have personally reviewed the images and the

resident's interpretation and concur with the above modified report.


The preceding interpretation was performed and electronically signed

by DR. CLIFF BERNSTEIN on 3/3/2018 1:11 PM

 

 

***Not Official Copy***:  CT:Chest W/non-ionic Contrast

Date/Time of Service:     March 02, 2018 21:35

Result status:                 Final

Result title:                    CT CHEST WITH IV CONTRAST

Contributor System:       RADIOLOGY
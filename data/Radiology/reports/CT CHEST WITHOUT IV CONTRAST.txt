Exam: CT CHEST WITHOUT IV CONTRAST


CLINICAL HISTORY: Female, 73 years old.

POSSIBLE PNEUMONIA

REASON: EVAL FOR INFILTRATE.

.


TECHNIQUE: Axial CT images were obtained from the thoracic inlet to

the adrenals without IV contrast. Sagittal and coronal reformations

were obtained.


COMPARISON: Chest x-ray from earlier the same day and CT chest dated

9/19/2014.


FINDINGS:

BASE OF NECK: A 3 mm calcification is seen in the right thyroid lobe.

Otherwise the thyroid is unremarkable.


LUNGS/PLEURA: There is biapical pleural-parenchymal thickening with

upper lobe predominant centrilobular emphysema. There is no

pneumothorax. There are large bilateral pleural effusions with small

bibasilar consolidations. There is also diffuse bronchial wall

thickening, intralobular septal thickening, and mild centrilobular

groundglass opacification, likely representing alveolar edema.   

LARGE AIRWAYS: Patent.

VESSELS: Normal caliber thoracic aorta and included abdominal aorta.

Moderate atherosclerotic calcification of the aortic arch and

descending thoracic aorta.

HEART: Enlarged without pericardial effusion.

MEDIASTINUM AND HILA: There is a mildly prominent prevascular lymph

node measuring 1.2 cm in diameter, enlarged when compared to prior

study and likely reactive.

AXILLAE: No lymphadenopathy.


BONES: Multilevel degenerative changes of the thoracic spine without

acute compression fracture.


UPPER ABDOMEN: The 1.5 cm hypodensity is seen in the right lobe of

the liver, similar compared to prior study. A few other small

hypodensities are noted throughout the liver, similar when compared

to study but poorly characterized without intravenous contrast. Small

hiatal hernia.


 

IMPRESSION:

Cardiomegaly with large bilateral pleural effusions, small bibasilar

atelectasis/consolidations, interlobular septal thickening, and

centrilobular ground glass opacification. Together these findings

suggest interstitial and alveolar edema. A superimposed infectious

process is not excluded and clinical correlation is recommended.


I, DR. DAVID AREMAN, have personally reviewed the images and the

resident's interpretation and concur with the above modified report.


The preceding interpretation was performed and electronically signed

by DR. DAVID AREMAN on 3/5/2018 4:03 PM


***Not Official Copy***:  Chest,routine(PA/AP/LAT)

Date/Time of Service:     March 06, 2018 10:11

Result status:                 Final

Result title:                    CHEST ROUTINE PA/AP AND LATERAL

Contributor System:       RADIOLOGY
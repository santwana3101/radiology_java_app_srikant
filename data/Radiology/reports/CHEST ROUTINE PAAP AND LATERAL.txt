Exam: CHEST ROUTINE PA/AP AND LATERAL


CLINICAL HISTORY: Female, 89 years old.

S/P PPM

REASON: LEAD PLACEMENT POST IMPLANT, PENDING DISCHARGE.

.


TECHNIQUE: Two views of the Chest, PA and Lateral


COMPARISON: Study from the previous day.


FINDINGS:

Small bilateral pleural effusions with adjacent atelectasis, slightly

worse..

There is no pneumothorax.

The cardiomediastinal silhouette is enlarged, stable.. Left chest

wall pacemaker is again seen with the leads overlying the right

ventricle. Status post median sternotomy.

 

IMPRESSION:

Persistent cardiomegaly.

Small pleural effusions with adjacent atelectasis, slightly worse.

 

***Not Official Copy***:  Chest,AP Portable

Date/Time of Service:     February 28, 2018 16:40

Result status:                 Final

Result title:                    CHEST AP PORTABLE

Contributor System:       RADIOLOGY
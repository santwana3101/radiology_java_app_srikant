import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import java.util.*;
import java.lang.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class NewFXMain_test_rm_negcontext extends Application {
    String keypath = "";
    String filepath = "";
	String outpath = "";
	ArrayList<String> keywords = new ArrayList<String>();
	ArrayList<String> pdfList = new ArrayList<String>();
	ArrayList<String> pdfTitles = new ArrayList<String>();
	//Positive and negative processing
	List<String> negPhrases = negPhrases_processing();
	List<List<String>> negPhrases_two_d = negPhrases_two_d_processing(negPhrases);
	List<String> conjunctions = conjunctions_processing();
	List<List<String>> conjunctions_two_d = negPhrases_two_d_processing(conjunctions);	
	BufferedWriter bw = null;
	FileWriter fw = null;
	//private static final String FILENAME = "C:\\test\\filename.txt";
	
	private Desktop desktop = Desktop.getDesktop();
    @Override
    public void start(Stage stage) {

        WebView browser = new WebView();
        WebEngine webEngine = browser.getEngine();
        //webEngine.load(getClass().getResource("Radiology-test1.js").toString());

		//Imported code
		stage.setTitle("File Chooser ");
 
        final FileChooser fileChooser = new FileChooser();
 
        final Button openButton = new Button("Select Keyword...");
        final Button openMultipleButton = new Button("Select Reports...");
		
		DirectoryChooser directoryChooser = new DirectoryChooser();
		final Button openDirectory = new Button("Select Directory...");
		
		//Curr
        Button btn = new Button();
        btn.setText("Submit");
		
        btn.setOnAction(new EventHandler<ActionEvent>() {        
            @Override
            public void handle(ActionEvent event) {
            	
               printMsg(keypath);
               for(String keyword:keywords)
            	   printMsg(keyword+", ");
               for(String title:pdfTitles)
            	   printMsg(title+", ");
               for(String content:pdfList)
            	   printMsg(content+", ");
           	   outpath = outpath.replace("\\","\\\\");
           	   printMsg("outpath:"+outpath);
               buildContent();
            }
        });
		
		//Imported
		openButton.setOnAction(
            new EventHandler<ActionEvent>() {
                @Override
                public void handle(final ActionEvent e) {
                    File file = fileChooser.showOpenDialog(stage);
                    if (file != null) {
                        keypath = (String)file.toPath().toString();
                        try
                        {
                            Scanner file1 = new Scanner(new File(keypath));

                            while (file1.hasNextLine())
                            {
                                String line = file1.nextLine();
                                keywords.add(line);
                            }

                            file1.close();
                        }
                        catch (Exception fnfe)
                        {
                            System.out.println("File not found");
                        }
                    }
                }
            });
 
        openMultipleButton.setOnAction(
            new EventHandler<ActionEvent>() {
                @Override
                public void handle(final ActionEvent e) {
                    List<File> list =
                        fileChooser.showOpenMultipleDialog(stage);
                    if (list != null) {
                        for (File file : list) {
                        	filepath = (String)file.toPath().toString();
                        	 try
                        	    {
                        	        Scanner file1 = new Scanner(new File(filepath));
        							StringBuffer content = new StringBuffer("");
                        	        while (file1.hasNextLine())
                        	        {
                        	            content.append(file1.nextLine());
                        	        }

                        	        file1.close();
                        	        String fn = filepath.substring(filepath.lastIndexOf('\\')+1);
                        	        pdfList.add(content.toString());
                        	        pdfTitles.add(fn);
                        	    }
                        	    catch (Exception fnfe)
                        	    {
                        	        System.out.println("File not found");
                        	    }
                        }
                    }
                }
            });
 
		openDirectory.setOnAction(
            new EventHandler<ActionEvent>() {
                @Override
                public void handle(final ActionEvent e) {
                    File directory = directoryChooser.showDialog(stage);
                    if (directory != null) {
                    	outpath = (String)directory.toPath().toString();
                    }
                }
            });

        final GridPane inputGridPane = new GridPane();
 
        GridPane.setConstraints(openButton, 0, 0);
        GridPane.setConstraints(openMultipleButton, 1, 0);
		GridPane.setConstraints(openDirectory, 2, 0);
		GridPane.setConstraints(btn, 3, 0);
        inputGridPane.setHgap(6);
        inputGridPane.setVgap(6);
        inputGridPane.getChildren().addAll(openButton, openMultipleButton, openDirectory, btn);
 
        final Pane rootGroup = new VBox(12);
        rootGroup.getChildren().addAll(inputGridPane);
        rootGroup.setPadding(new Insets(12, 12, 12, 12));
 
        stage.setScene(new Scene(rootGroup));
        stage.show();
    }
    
	
	
    public void printMsg(String s){
        System.out.println(s);
    }
    
    
    
    //BuildContent
    

    public void buildContent()
	{
		String tableBody="";
	    for (int i=0; i < pdfList.size(); i++){
			tableBody = "<head><style>table {border: 1px solid black; "+
			"font-family: arial, sans-serif; "+
			"border-collapse: collapse; "+
			"width: 100%; "+
			"} "+
			"td, th { "+
			"border: 1px solid black; "+
			"text-align: left; "+
			"padding: 8px; "+
			"} "+
			"tr:nth-child(even) { "+
			"background-color: #dddddd; "+
			"}</style></head><body>";
			tableBody += "<table width='100%' bgcolor='white'>";
	    	tableBody += "<tr><td>" + highlightBody(pdfList.get(i), keywords) + "</td></tr>";
			tableBody += "</table></body>";
			printMsg(""+i);
			System.out.println();
			printMsg(tableBody);
			
			try {
				//fw = new FileWriter(FILENAME);
				String fn_name = pdfTitles.get(i).split("\\.")[0];
				fw = new FileWriter(outpath+"\\\\"+fn_name+".doc");
				bw = new BufferedWriter(fw);
				bw.write(tableBody);
				System.out.println("Done");

			} catch (IOException e) {

				e.printStackTrace();

			} finally {

				try {

					if (bw != null)
						bw.close();

					if (fw != null)
						fw.close();

				} catch (IOException ex) {

					ex.printStackTrace();

				}

			}
	    }	
	}


    /*public int indexOfIgnoreCase(stringinput ,searchvalue, start) {
    	String input;
    	String search;
    	int returnindex = -1;

    	if (stringinput == "" || stringinput is null) {
    		return returnindex;	
    	}
    	else { 
    		input = stringinput.toString().toLowerCase();
    	}

    	if (searchvalue == "" || searchvalue is null) {
        	return returnindex;
    	}
    	else {
    		search = searchvalue.toString().toLowerCase();
    	}
    	//console.log("ip:"+input);
    	//console.log("srch:"+search);
    	start = start || 0;
    	//Note - start will also be 0 for null, undefined, 0, false, ''
    	returnindex = input.indexOf(search, start);

    	return returnindex;
    }*/
    
    //Highlight Part functions
    
    public String highlightBody(String pdfcontent, ArrayList<String> keywords)
    {
    	String bodyText = pdfcontent;
    	ArrayList<String> rep_sentences = break_sentences(bodyText, ".");
    	printMsg("Inside highlight body:rep_sentences");
    	for(String sent:rep_sentences)
    		printMsg(sent);
    	System.out.println();
    	System.out.println();
    	System.out.println();
    	System.out.println();
    	bodyText="";
    	for(int i = 0;i < rep_sentences.size(); i++)
    		bodyText += highlightSent(rep_sentences.get(i), keywords);
    	return bodyText;
    }

    public String highlightSent(String rep_sentence, ArrayList<String> keywords)
    {
    	  String sentText = rep_sentence;
    	  sentText = check_comorb_in_sentences(rep_sentence, keywords, negPhrases_two_d, conjunctions_two_d);
    	  printMsg("Sent text to highlightSent:"+sentText);
    	  return sentText;

    }

    public String highlightWord(String pdfContent, ArrayList<String> keywords)
    {
      String bodyText=pdfContent;

      for (int i = 0; i < keywords.size(); i++) {
        bodyText = doHighlight(bodyText, keywords.get(i));
      }

      //container.html(bodyText);

      return bodyText;
    }

    public String doHighlight(String sentence, String searchTerm) 
    {
    	printMsg("In dohighlight:sentence:"+sentence+"keyword:"+searchTerm);
      String highlightStartTag = "<span style='color:blue; background-color:yellow;'>";
      String highlightEndTag = "</span>";

      String newText = "";
      int i = -1;
      int j = -1;
      String lcSearchTerm = searchTerm.toLowerCase();
      String lcBodyText = sentence.toLowerCase();
      int negFlag = 0; 
      
      
      while (sentence.length() > 0) {
    	for(String negphrase:negPhrases) {
    		if (isAWordMatch(lcBodyText, negphrase)) {
				printMsg("Negative phrase present is :"+negphrase);
				j = lcBodyText.indexOf(negphrase, j+1);
				break;
			}
    	}
        i = lcBodyText.indexOf(lcSearchTerm, i+1);
        if(j >= 0 && j < i)
        	negFlag = 1;
        else
        	negFlag = 0;
        if (i < 0) {
          newText += sentence;
          sentence = "";
        } else {
          if (sentence.lastIndexOf(">", i) >= sentence.lastIndexOf("<", i)) {
            if (lcBodyText.lastIndexOf("/script>", i) >= lcBodyText.lastIndexOf("<script", i)) {
            	System.out.println("i:"+i+"srchtrm len:"+searchTerm.length()+"j:"+j+"negflg:"+negFlag);
              if(negFlag == 0)
            	  newText += sentence.substring(0, i) + highlightStartTag + sentence.substring(i, i+searchTerm.length()) + highlightEndTag;
              else
            	  newText += sentence.substring(0, i) + sentence.substring(i, i+searchTerm.length()) ;
              System.out.println("*****"+newText);
              sentence = sentence.substring(i + searchTerm.length());

              lcBodyText = sentence.toLowerCase();

              i = -1;

            }

          }

        }

      }

      return newText;
    }
    
    public boolean checkSuffix(String sentence, String comorb)
    {
    	sentence = sentence.toLowerCase();
    	comorb = comorb.toLowerCase();
    	int index = sentence.indexOf(comorb);
    	int startIndex = index;
    	int endIndex = index;
    	while (startIndex > 0)
    	{
    		char refChar = sentence.charAt(startIndex - 1);
    		if (refChar == ' ' || 
    			refChar == -1 || 
    			refChar == ',' ||
    			refChar == '.')
    			break;
    		else
    			startIndex = startIndex - 1;
    	}

    	while (endIndex < sentence.length() - 1)
    	{
    		char refChar = sentence.charAt(endIndex + 1);
    		if (refChar == ' ' || 
    			refChar == -1 ||
    			refChar == ',' ||
    			refChar == '.')
    			break;
    		else
    			endIndex = endIndex + 1;
    	}
    	String derivedComorb =  sentence.substring(startIndex, endIndex + 1);
    	//if (comorb.search(" ") < 0 && derivedComorb.search(comorb) == 0)
    	return true;
    	//console.log("return false");
    	//return false;
    }

    //determine that the keyword must be highlighted or not

    public String check_comorb_in_sentences(String impression_sentence, ArrayList<String> final_comorb, List<List<String>> negPhrases_two_d, List<List<String>> conjunctions_two_d){
    	printMsg("For sentence:"+impression_sentence);
    	int i = 0, j = 0, k = 0;
    	String temp_sentence ="" ;
    	int comorb_parts_present=0;
    		for (j = 0; j < final_comorb.size(); j++){ 	
    			comorb_parts_present = 0;
    				if ((impression_sentence.indexOf(final_comorb.get(j)) != -1) || (impression_sentence.toLowerCase().indexOf(final_comorb.get(j).toLowerCase()) != -1)){
    					//console.log("in check_comorb_in_sentences->for->if:"+impression_sentence+" "+final_comorb[i]);
    					if (checkSuffix(impression_sentence, final_comorb.get(j)))
    					{
    						//console.log("in check_comorb_in_sentences->if->if:"+impression_sentence+" "+final_comorb);
    						comorb_parts_present = 1;
    					}
    					else
    					{
    						comorb_parts_present = 0;
    					}	
    				}
    				else {
    					comorb_parts_present = 0;
    				}
    			if (comorb_parts_present == 1){
    				temp_sentence = impression_sentence;
    				break;
    			}
    		}

        System.out.println("####Sentence:"+impression_sentence);
        System.out.println("comorb_parts_present:"+comorb_parts_present);
    	if(comorb_parts_present == 0) {
    		printMsg("Bare Return value is:"+impression_sentence);
    		return impression_sentence;
    	}
    	int neg_phrase_present = 1;
    	int conj_present = 1;
    		for (j = 0; j < negPhrases_two_d.size(); j++){
    			neg_phrase_present = 1;
    			for (k = 0; k < negPhrases_two_d.get(j).size(); k++){
    				if (!isAWordMatch(temp_sentence, negPhrases_two_d.get(j).get(k))) {
    					printMsg("Matcher:"+negPhrases_two_d.get(j).get(k));
    					neg_phrase_present = 0;
    					break;
    				}
    			}
    			if (neg_phrase_present == 1)
    				break;
    		}
            System.out.println("neg_phrase_present:"+neg_phrase_present);
    		if (neg_phrase_present == 1){	
    			for (j = 0; j < conjunctions_two_d.size(); j++){
    				conj_present = 1;
    				for (k = 0; k < conjunctions_two_d.get(j).size(); k++){
    					if (!isAWordMatch(temp_sentence, conjunctions_two_d.get(j).get(k))) {
    						conj_present = 0;
    						break;
    					}
    				}
    				if (conj_present == 1){
    					break;
    				}
    			}
    			if(conj_present == 1){
    				//+ve and -ve both present
    				temp_sentence= highlightWord(temp_sentence, keywords);
    			}
    			temp_sentence= highlightWord(temp_sentence, keywords);
    		}
    		else{
    			//no -ve
    			printMsg("Only positive context keyword present:"+temp_sentence);
    			temp_sentence= highlightWord(temp_sentence, keywords);
    		}
    	printMsg("Return value is:"+temp_sentence);
    	return temp_sentence;
    }
    
    
    //Static Processing functions
    
    public ArrayList conjunctions_processing(){
    	ArrayList<String> conjunctions = new ArrayList<String>();
    	
    	//conjunctions.push("but");
    	
    	conjunctions.add("however");
    	conjunctions.add("nevertheless");
    	conjunctions.add("yet");
    	conjunctions.add("though");
    	conjunctions.add("although");
    	conjunctions.add("still");
    	conjunctions.add("aside from");
    	conjunctions.add("except");
    	conjunctions.add("apart from");
    	conjunctions.add("secondary to");
    	conjunctions.add("as the cause of");
    	conjunctions.add("as the source of");
    	conjunctions.add("as the reason of");
    	conjunctions.add("as the etiology of");
    	conjunctions.add("as the origin of");
    	conjunctions.add("as the cause for");
    	conjunctions.add("as the source for");
    	conjunctions.add("as the reason for");
    	conjunctions.add("as the etiology for");
    	conjunctions.add("as the origin for");
    	conjunctions.add("as the secondary cause of");
    	conjunctions.add("as the secondary source of");
    	conjunctions.add("as the secondary reason of");
    	conjunctions.add("as the secondary etiology of");
    	conjunctions.add("as the secondary origin of");
    	conjunctions.add("as the secondary cause for");
    	conjunctions.add("as the secondary source for");
    	conjunctions.add("as the secondary reason for");
    	conjunctions.add("as the secondary etiology for");
    	conjunctions.add("as the secondary origin for");
    	conjunctions.add("as a cause of");
    	conjunctions.add("as a source of");
    	conjunctions.add("as a reason of");
    	conjunctions.add("as a etiology of");
    	conjunctions.add("as a cause for");
    	conjunctions.add("as a source for");
    	conjunctions.add("as a reason for");
    	conjunctions.add("as a etiology for");
    	conjunctions.add("as a secondary cause of");
    	conjunctions.add("as a secondary source of");
    	conjunctions.add("as a secondary reason of");
    	conjunctions.add("as a secondary etiology of");
    	conjunctions.add("as a secondary origin of");
    	conjunctions.add("as a secondary cause for");
    	conjunctions.add("as a secondary source for");
    	conjunctions.add("as a secondary reason for");
    	conjunctions.add("as a secondary etiology for");
    	conjunctions.add("as a secondary origin for");
    	conjunctions.add("cannot rule out"); // double negation
        conjunctions.add("cause of");
    	conjunctions.add("cause for");
    	conjunctions.add("causes of");
    	conjunctions.add("causes for");
    	conjunctions.add("source of");
    	conjunctions.add("source for");
    	conjunctions.add("sources of");
    	conjunctions.add("sources for");
    	conjunctions.add("reason of");
    	conjunctions.add("reason for");
    	conjunctions.add("reasons of");
    	conjunctions.add("reasons for");
    	conjunctions.add("etiology of");
    	conjunctions.add("etiology for");
    	conjunctions.add("trigger event for");
    	conjunctions.add("origin of");
    	conjunctions.add("origin for");
    	conjunctions.add("origins of");
    	conjunctions.add("origins for");
    	conjunctions.add("other possibilities of");
    	
    	
    	return conjunctions;
    }

    public ArrayList negPhrases_processing(){
    	ArrayList<String> negPhrases = new ArrayList<String>();

    	negPhrases.add("absence of");
    	negPhrases.add("cannot see");
    	negPhrases.add("cannot");
    	negPhrases.add("checked for");
    	negPhrases.add("declined");
    	negPhrases.add("declines");
    	negPhrases.add("denied");
    	negPhrases.add("denies");
    	negPhrases.add("denying");
    	negPhrases.add("evaluate for");
    	negPhrases.add("fails to reveal");  
    	negPhrases.add("free of");
    	negPhrases.add("negative for");
    	negPhrases.add("never developed");
    	negPhrases.add("never had");
    	negPhrases.add("no");  
    	negPhrases.add("no abnormal");
    	negPhrases.add("no cause of");
    	negPhrases.add("no complaints of");
    	negPhrases.add("no evidence");
    	negPhrases.add("no new evidence");
    	negPhrases.add("no other evidence");
    	negPhrases.add("no evidence to suggest");
    	negPhrases.add("no findings of");
    	negPhrases.add("no findings to indicate");
    	negPhrases.add("no mammographic evidence of");
    	negPhrases.add("no new");
    	negPhrases.add("no radiographic evidence of");
    	negPhrases.add("no sign of");
    	negPhrases.add("no significant");  
    	negPhrases.add("no signs of"); 
    	negPhrases.add("no suggestion of");
    	negPhrases.add("no suspicious");
    	negPhrases.add("not");  
    	negPhrases.add("not appear");
    	negPhrases.add("not appreciate");
    	negPhrases.add("not associated with");
    	negPhrases.add("not complain of");
    	negPhrases.add("not demonstrate");
    	negPhrases.add("not exhibit");
    	negPhrases.add("not feel");
    	negPhrases.add("not had");
    	negPhrases.add("not have");
    	negPhrases.add("not know of");
    	negPhrases.add("not known to have");
    	negPhrases.add("not reveal");
    	negPhrases.add("not see");
    	negPhrases.add("not to be");
    	negPhrases.add("patient was not");
    	negPhrases.add("rather than");
    	negPhrases.add("resolved");
    	negPhrases.add("test for");
    	negPhrases.add("to exclude");
    	negPhrases.add("unremarkable for");
    	negPhrases.add("with no");
    	negPhrases.add("without any evidence of");
    	negPhrases.add("without evidence");
    	negPhrases.add("without indication of");
    	negPhrases.add("without sign of");
    	negPhrases.add("without");
    	negPhrases.add("rule out for");
    	negPhrases.add("rule him out for");
    	negPhrases.add("rule her out for");
    	negPhrases.add("rule the patient out for");
    	negPhrases.add("rule him out");
    	negPhrases.add("rule her out");
    	negPhrases.add("rule out");
    	negPhrases.add("r/o");
    	negPhrases.add("ro");
    	negPhrases.add("rule the patient out");
    	negPhrases.add("rules out");
    	negPhrases.add("rules him out");
    	negPhrases.add("rules her out");
    	negPhrases.add("ruled the patient out for");
    	negPhrases.add("rules the patient out");
    	negPhrases.add("ruled him out against");
    	negPhrases.add("ruled her out against");
    	negPhrases.add("ruled him out");
    	negPhrases.add("ruled her out");
    	negPhrases.add("ruled out against");
    	negPhrases.add("ruled the patient out against");
    	negPhrases.add("did rule out for");
    	negPhrases.add("did rule out against");
    	negPhrases.add("did rule out");
    	negPhrases.add("did rule him out for");
    	negPhrases.add("did rule him out against");
    	negPhrases.add("did rule him out");
    	negPhrases.add("did rule her out for");
    	negPhrases.add("did rule her out against");
    	negPhrases.add("did rule her out");
    	negPhrases.add("did rule the patient out against");
    	negPhrases.add("did rule the patient out for");
    	negPhrases.add("did rule the patient out");
    	negPhrases.add("can rule out for");
    	negPhrases.add("can rule out against");
    	negPhrases.add("can rule out");
    	negPhrases.add("can rule him out for");
    	negPhrases.add("can rule him out against");
    	negPhrases.add("can rule him out");
    	negPhrases.add("can rule her out for");
    	negPhrases.add("can rule her out against");
    	negPhrases.add("can rule her out");
    	negPhrases.add("can rule the patient out for");
    	negPhrases.add("can rule the patient out against");
    	negPhrases.add("can rule the patient out");
    	negPhrases.add("adequate to rule out for");
    	negPhrases.add("adequate to rule out");
    	negPhrases.add("adequate to rule him out for");
    	negPhrases.add("adequate to rule him out");
    	negPhrases.add("adequate to rule her out for");
    	negPhrases.add("adequate to rule her out");
    	negPhrases.add("adequate to rule the patient out for");
    	negPhrases.add("adequate to rule the patient out against");
    	negPhrases.add("adequate to rule the patient out");
    	negPhrases.add("sufficient to rule out for");
    	negPhrases.add("sufficient to rule out against");
    	negPhrases.add("sufficient to rule out");
    	negPhrases.add("sufficient to rule him out for");
    	negPhrases.add("sufficient to rule him out against");
    	negPhrases.add("sufficient to rule him out");
    	negPhrases.add("sufficient to rule her out for");
    	negPhrases.add("sufficient to rule her out against");
    	negPhrases.add("sufficient to rule her out");
    	negPhrases.add("sufficient to rule the patient out for");
    	negPhrases.add("sufficient to rule the patient out against");
    	negPhrases.add("sufficient to rule the patient out");
        negPhrases.add("what must be ruled out is");
        
        negPhrases.add("should be ruled out for");
    	negPhrases.add("ought to be ruled out for");
        negPhrases.add("may be ruled out for");
    	negPhrases.add("might be ruled out for");
    	negPhrases.add("could be ruled out for");
    	negPhrases.add("will be ruled out for");
    	negPhrases.add("can be ruled out for");
    	negPhrases.add("must be ruled out for");
    	negPhrases.add("is to be ruled out for");
    	negPhrases.add("be ruled out for");
    	negPhrases.add("unlikely");
    	negPhrases.add("free");
    	negPhrases.add("was ruled out");
    	negPhrases.add("is ruled out");
    	negPhrases.add("are ruled out");
    	negPhrases.add("have been ruled out");
    	negPhrases.add("has been ruled out");
    	negPhrases.add("being ruled out");  
    	negPhrases.add("should be ruled out");
    	negPhrases.add("ought to be ruled out");
    	negPhrases.add("may be ruled out");
    	negPhrases.add("might be ruled out");
    	negPhrases.add("could be ruled out");
    	negPhrases.add("will be ruled out");
    	negPhrases.add("can be ruled out");
    	negPhrases.add("must be ruled out");
    	negPhrases.add("is to be ruled out");
    	negPhrases.add("be ruled out");
    	
    	return negPhrases;

    }

    public ArrayList break_sentences(String data, String delimiter_string){
    	String file_content = data;
    	ArrayList<String> textfile_sentences = new ArrayList<String>();
    	while (file_content.length() != 0){
    			int pos_newline = file_content.indexOf(delimiter_string);
    			if (pos_newline == -1){                
    				textfile_sentences.add(file_content.substring(0, file_content.length()));
    				break;
    			}
    			else if(pos_newline == 0){
    				file_content = file_content.substring(1, file_content.length());
    			}
    			else{
    				textfile_sentences.add(file_content.substring(0, pos_newline));
    				file_content = file_content.substring((pos_newline) + 1, file_content.length());
    			}
    	}
    	return textfile_sentences;
    }

    public List<List<String>> negPhrases_two_d_processing(List<String> negPhrases){
    	List<List<String>> negPhrases_two_d = new ArrayList<List<String>>();
    	int i = 0;
    	for (i = 0;i < negPhrases.size(); i++){
    		List<String> temp_array = new ArrayList<String>();
    		temp_array = break_sentences(negPhrases.get(i)," ");
    		negPhrases_two_d.add(temp_array);
    	}
    	return negPhrases_two_d;
    }
    
    public boolean isAWordMatch(String searchOnString, String searchText) {
    	  //searchText = searchText.replace("/[-/\\^$*+?.()|[]{}]/g", "\\$&");
    	  return Pattern.compile("(?i)\\b"+searchText+"\\b").matcher(searchOnString).find();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
}